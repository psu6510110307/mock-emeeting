// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getStorage } from "firebase/storage"
import { getAuth, signInWithEmailAndPassword, signOut} from "firebase/auth";
import { getDatabase, set, ref, update} from "firebase/database";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAPPurnapXdSbllfARcIds9JlgC4Uayl7M",
  authDomain: "emeetfes.firebaseapp.com",
  projectId: "emeetfes",
  storageBucket: "emeetfes.appspot.com",
  messagingSenderId: "632905686306",
  appId: "1:632905686306:web:79482c59e442e6f3d0597b",
  measurementId: "G-L0160MBSJP"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
const auth = getAuth();
const database = getDatabase();
const email: string = (document.getElementById('email') as HTMLInputElement).value;
const password: string = (document.getElementById('psw') as HTMLInputElement).value;

  signInWithEmailAndPassword(auth, email, password)
  .then((userCredential) => {
    // Signed in
    const user = userCredential.user;

    // Save login details into real-time database
    const lgDate = new Date();
    update(ref(database, `users/${user.uid}`), {
      last_login: lgDate,
    })
      .then(() => {
        // Data saved successfully!
        alert("User logged in successfully");
      })
      .catch((error) => {
        // The write failed...
        alert(error);
      });
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    alert(errorMessage);
  });

signOut(auth).then(() => {
  // Sign-out successful.
}).catch((error: any) => {
  // An error happened.
});

export const storage = getStorage()