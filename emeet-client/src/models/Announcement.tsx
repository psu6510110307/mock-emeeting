export default interface Announcement{
    id: number
    topic: string
    meetDate: string
    pubDateTime: Date
    userCode: string
}